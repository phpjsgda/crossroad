var crossroad = {
    settings: {
        lightInterval: 500,
        lampInterval: 1000,
        dataUrl: '/api/lights'
    },
    pulseLamps:[],
    init: function () {
        crossroad.suspendAllLights();
        setInterval(crossroad.getData, crossroad.settings.lightInterval);
        setInterval(crossroad.makePulsation, crossroad.settings.lampInterval);
    },
    offLight: function (light) {
        light.find('.lamp').each(function (){
            crossroad.removeLampFromPulsation($(this));
            $(this).removeClass('lampOn');
        });
    },
    suspendAllLights: function () {
        $('.light').each(function (){
            crossroad.suspendLight($(this));
        });
    },
    suspendLight: function (light) {
        crossroad.offLamp(light, 'green');
        crossroad.offLamp(light, 'red');
        crossroad.addLampToPulsation(light.find('.yellow'));
    },
    addLampToPulsation: function (lamp){
        var id = lamp.attr('id');
        if (jQuery.inArray( id, crossroad.pulseLamps ) === -1){
            crossroad.pulseLamps.push(id);
        }
    },
    removeLampFromPulsation: function (lamp){
        var id = lamp.attr('id');
        crossroad.pulseLamps = jQuery.grep(crossroad.pulseLamps, function(value) {
            return value != id;
        });
    },
    makePulsation: function () {
        $.each(crossroad.pulseLamps, function ( prop, val ) {
            var lamp = $('#'+val);
            if (lamp.hasClass('lampOn')){
                lamp.removeClass('lampOn');
            }else{
                lamp.addClass('lampOn');
            }
        });
    },
    offLamp: function (light, lamp) {
        light.find('.' + lamp).removeClass('lampOn');
    },
    onLamp: function (light, lamp) {
        light.find('.' + lamp).addClass('lampOn');
    },
    processData: function (data){
        $.each(data, function ( prop, val ) {
           var light = $('#light' + val.id);

            switch (val.state)
            {
                case 'shutdown':
                    crossroad.offLight(light);
                    break;
                case 'suspended':
                    crossroad.suspendLight(light);
                    break;
                case 'red_light':
                    crossroad.offLight(light);
                    crossroad.onLamp(light, 'red');
                    break;
                case 'green_light':
                    crossroad.offLight(light);
                    crossroad.onLamp(light, 'green');
                    break;
                case 'yellow_light':
                    crossroad.offLight(light);
                    crossroad.onLamp(light, 'yellow');
                    break;
                case 'red_yellow_light':
                    crossroad.offLight(light);
                    crossroad.onLamp(light, 'red');
                    crossroad.onLamp(light, 'yellow');
                    break;
                default:
                    console.log('Niezdefiniowane akcja. Wylaczam sygnalizację!');
                    crossroad.suspendAllLights();
            }
        });
    },
    getData: function (){
        $.ajax({
            type: 'GET',
            url: crossroad.settings.dataUrl,
            dataType: 'json',
            beforeSend: function (request) {
                request.setRequestHeader('X-CROSSROAD-AUTH', 'MKJGYUgniuhIUOuygbiuFiytvURDUrdb')
            }
        }).done(function (json) {
            crossroad.processData(json.data);
        }).fail(function () {
            console.log("nie udalo sie załadować danych");
            crossroad.suspendAllLights();
        });
    }
};

$(document).ready(function () {
    crossroad.init();
});
