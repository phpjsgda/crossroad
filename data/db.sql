DROP TABLE IF EXISTS 'lights'; 

CREATE TABLE `lights` (
  `id` int(6) NOT NULL AUTO_INCREMENT, 
  `state` enum('shutdown','suspended','red_light','green_light','yellow_light','red_yellow_light') NOT NULL DEFAULT 'suspended' ,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;