<?php

namespace Sda\Crossroad\Controller;

use Sda\Crossroad\Config\Routing;
use Sda\Crossroad\Light\LightBuilder;
use Sda\Crossroad\Light\LightNotFoundException;
use Sda\Crossroad\Light\LightRepository;
use Sda\Crossroad\Light\Validator;
use Sda\Crossroad\Request\Request;
use Sda\Crossroad\Response\Response;

/**
 * Class ApiController
 * @package Sda\Crossroad\Controller
 */
class ApiController
{
    /**
     * @var LightRepository
     */
    private $lightRepository;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Response
     */
    private $response;

    /**
     * ApiController constructor.
     * @param LightRepository $lightRepository
     * @param Request $request
     * @param Response $response
     */
    public function __construct(
        LightRepository $lightRepository,
        Request $request,
        Response $response
    ) {
        $this->lightRepository = $lightRepository;
        $this->request = $request;
        $this->response = $response;
    }

    public function run()
    {
        $action = array_key_exists('action', $_GET) ? $_GET['action'] : Routing::LIGHT;

        switch ($action) {
            case Routing::LIGHT:
                if ($this->request->getHttpMethod() === 'POST') {

                    $builder = new LightBuilder();

                    $editedLight = $builder
                        ->withId((int)$this->request->getParamFromPost('id'))
                        ->withState($this->request->getParamFromPost('state'))
                        ->build();
                    /*
                     * Wymiennie możemy użyc takiej konstrukcji
                     * $light = new Light(
                     *      (int)$this->request->getParamFromPost('id'),
                     *      $this->request->getParamFromPost('state')
                     * );
                     * */

                    $validate = new Validator();
                    if ($validate->validate($editedLight) === true) {
                        $this->lightRepository->saveLight($editedLight);
                        $this->response->send('OK');
                    } else {
                        $this->response->send('Niepoprawne dane wprowadzone do obiektu', Response::STATUS_BAD_REQUEST);
                    }

                } elseif ($this->request->getHttpMethod() === 'GET') {

                    $id = (int)$this->request->getParamFromGet('id', 0);

                    if (is_numeric($id) && $id > 0) {
                        try {
                            $this->response->send(
                                $this->lightRepository->getLight($id)
                            );
                        } catch (LightNotFoundException $e) {
                            $this->response->send('Nieodnaleziono obiektu Light o ID: ' . $id,
                                Response::STATUS_NOT_FOUND);
                        }
                    } else {
                        $this->response->send('Niepoprawny ID', Response::STATUS_BAD_REQUEST);
                    }
                }
                break;
            case Routing::LIGHTS:
                $this->response->send($this->lightRepository->getAllLights());
                break;
            default:
                $this->response->send('Niepoprawna akcja', Response::STATUS_NOT_FOUND);
                break;
        }
    }
}