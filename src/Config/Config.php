<?php

namespace Sda\Crossroad\Config;

class Config
{
    const DB_CONNECTION_DATA = [
        'dbname' => 'crossroad2',
        'user' => 'root',
        'password' => '',
        'host' => 'localhost',
        'driver' => 'pdo_mysql'
    ];
}
