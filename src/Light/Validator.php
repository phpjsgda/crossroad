<?php

namespace Sda\Crossroad\Light;

/**
 * Class Validator
 * @package Sda\Crossroad\Light
 */
class Validator
{
    const DATA_TO_CHECK = [
        'shutdown',
        'suspended',
        'red_light',
        'green_light',
        'yellow_light',
        'red_yellow_light'
    ];

    /**
     * @param Light $light
     * @return bool
     */
    public function validate(Light $light)
    {
        return is_numeric($light->getId()) && $light->getId() > 0 && in_array($light->getState(), self::DATA_TO_CHECK,
                true);
    }
}
