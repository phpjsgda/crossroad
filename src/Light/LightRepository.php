<?php

namespace Sda\Crossroad\Light;

use Doctrine\DBAL\Connection;

/**
 * Class LightRepository
 * @package Sda\Crossroad\Light
 */
class LightRepository
{
    /**
     * @var Connection
     */
    private $dbh;

    /**
     * LightRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param int $id
     * @return Light
     * @throws LightNotFoundException
     */
    public function getLight($id)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `lights` WHERE `id` = :id');
        $sth->bindValue('id', $id, \PDO::PARAM_INT);
        $sth->execute();

        $loginData = $sth->fetch();

        if (false === $loginData) {

           throw new LightNotFoundException();
        }

        $builder = new LightBuilder();

        return $builder
            ->withId((int)$loginData['id'])
            ->withState($loginData['state'])
            ->build();
    }

    /**
     * @return LightCollection
     */
    public function getAllLights()
    {
        $sth = $this->dbh->prepare('SELECT * FROM `lights`');
        $sth->execute();
        $loginData = $sth->fetchAll();
        $lights = new LightCollection();
        foreach($loginData as $row){

             $builder = new LightBuilder();
             $light = $builder
            ->withId((int)$row['id'])
            ->withState($row['state'])
            ->build();

             $lights->add($light);
         }
         return $lights;
    }

    /**
     * @param Light $light
     */
    public function saveLight(Light $light)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `lights` WHERE `id` = :id');
        $sth->bindValue('id', $light->getId(), \PDO::PARAM_INT);
        $sth->execute();
        $lightData = $sth->fetch();

        if(false === $lightData)
        {
            $this->dbh->insert('lights', ['id'=>$light->getId(), 'state'=>$light->getState()]);
        } else {
            $this->dbh->update('lights', ['state'=>$light->getState()], ['id'=>$light->getId()]);
        }
    }
}
