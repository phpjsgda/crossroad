<?php

namespace Sda\Crossroad\Light;

class Light implements \JsonSerializable {

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $state;

    /**
     * Light constructor.
     * @param int $id
     * @param string $state
     */
    public function __construct($id, $state)
    {
        $this->id = $id;
        $this->state = $state;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'state' => $this->state
        ];
    }
}
