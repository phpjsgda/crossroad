<?php

namespace Sda\Crossroad\Light;

class LightBuilder {

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $state = 'suspended';

    /**
     * @return Light
     */
    public function build()
    {
        return new Light(
            $this->id,
            $this->state
        );
    }

    /**
     * @param int $id
     * @return LightBuilder
     */
    public function withId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $state
     * @return LightBuilder
     */
    public function withState($state)
    {
        $this->state = $state;
        return $this;
    }
}
