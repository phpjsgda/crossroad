<?php

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Sda\Crossroad\Config\Config;
use Sda\Crossroad\Controller\ApiController;
use Sda\Crossroad\Light\LightRepository;
use Sda\Crossroad\Request\Request;
use Sda\Crossroad\Response\Response;

require_once __DIR__ . '/../vendor/autoload.php';


$config = new Configuration();
$request = new Request();
$response = new Response();
$dbh = DriverManager::getConnection(Config::DB_CONNECTION_DATA, $config);
$repo =new LightRepository($dbh);
$app = new ApiController($repo, $request, $response);
$app->run();
